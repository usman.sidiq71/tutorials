extends LinkButton

export(String) var scene_to_load

func _on_New_Game_pressed():
	get_tree().change_scene("res://Scenes/" + scene_to_load + ".tscn")

func _on_Stage_Select_pressed():
	get_tree().change_scene("res://Scenes/" + scene_to_load + ".tscn")
