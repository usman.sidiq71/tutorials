extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

onready var sprite = get_node("Sprite")

const UP = Vector2(0, -1)
var velocity = Vector2()

var on_ground = false
var jump_count = 0

func get_input():
	velocity.x = 0
	var direction = 0
	if Input.is_action_just_pressed('up'):
		if jump_count < 2:
			jump_count += 1
			velocity.y = jump_speed
			on_ground = false
	if Input.is_action_pressed('right'):
		velocity.x += speed
		direction = 1
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		direction = -1
	if Input.is_action_pressed('ui_select') and is_on_floor():
		speed = 600
	else:
		speed = 400
	
	if is_on_floor():
		on_ground = true
		jump_count = 0
	else:
		on_ground = false
	
	if	direction == 0:
		sprite.play("Idle")
	else:
		if on_ground == false:
			sprite.play("Jump")
		else:
			sprite.play("Walk")
	if velocity.x > 0:
		sprite.set_flip_h(false)
	elif velocity.x < 0:
		sprite.set_flip_h(true)	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
	